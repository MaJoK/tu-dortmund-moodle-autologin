const usernameField = document.getElementById('username');
const passwordField = document.getElementById('password');
const loginDelayField = document.getElementById('login-delay');

const defaultDelay = 2

function setStatus(text) {
  const status = document.getElementById('status');
  status.innerText = text;
  status.style.setProperty('color', undefined)
}
function setSuccessStatus(text) {
  const status = document.getElementById('status');
  status.innerText = text;
  status.style.setProperty('color', 'green')
}

function loadValues() {
  setStatus('Loading...');

  browser.storage.local.get({credentials: {username: '', password: ''}, loginDelay: defaultDelay * 1000}).then(storedData => {
    setStatus('');
    let {credentials, loginDelay} = storedData;

    usernameField.value = credentials.username;
    passwordField.value = credentials.password;
    loginDelayField.value = loginDelay / 1000;
  });
}

function storeValues() {
  let loginDelayVal = loginDelayField.value;
  if (loginDelayVal === null || loginDelayVal === undefined || typeof loginDelayVal === 'string' && loginDelayVal.trim() == '') {
    loginDelayField.value = defaultDelay;
  }
  let toStore = {
    credentials: {
      username: usernameField.value,
      password: passwordField.value,
    },
    auto_login_enabled: true,
    loginDelay: (loginDelayField.value-0)*1000
  }


  setStatus('Saving...')

  browser.storage.local.set(toStore)
    .then(() => setSuccessStatus('Saved'));
}


loadValues()

document.getElementById('form').addEventListener('submit', event => {
  event.preventDefault();
  storeValues();
});
