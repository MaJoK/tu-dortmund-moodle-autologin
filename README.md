# Automatic login for the Moodle of the TU-Dortmund.

## Description

If a page of the TU Dortmund University Moodle is visited (moodle.tu-dortmund.de), the extension checks whether your are logged in.
If not, after a short wait and the option to cancel, you will be redirected to the
the single sign-on page of the ITMC. On this page the extension can insert the login credentials, if they have been configured in the extension settings. If the browser enters the credentials automatically, they will simply be used. Then the login will continue and you will be redirected to back to original moodle page you wanted to visit.

## This Project

The extension simply the plain javascript code bundled up.
The auto_login.js is a content script, that will be run in the background of moodle and the login page.
