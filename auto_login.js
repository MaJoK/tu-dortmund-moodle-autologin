const ssoLoginPrefix =
  "https://sso.itmc.tu-dortmund.de/openam/XUI/?realm=/tudo&goto=https://sso.itmc.tu-dortmund.de:443/openam/oauth2/authorize?response_type%3Dcode%26redirect_uri%3Dhttps://moodle.tu-dortmund.de/auth/sso/index.php%26client_id%3Dmoodle-lms-production"
const moodle = {
  getLogInHref() {
    return document.body.querySelector(
      'a[href="https://moodle.tu-dortmund.de/login/index.php"]'
    );
  },

  notLoggedIn() {
    return !!this.getLogInHref() || this.isLoginPage();
  },

  getLogInInitateURL() {
    return "https://moodle.tu-dortmund.de/auth/sso/index.php";
  },

  isLoginPage() {
    return document.location.href.startsWith(
      "https://moodle.tu-dortmund.de/login/index.php"
    );
  },
};

function insertAutoLoginPanel(cancel, loginDelayMS) {
  const template = document.createElement("template");
  template.innerHTML = `
    <div id="autologin-panel">
      <span id="autologin-message"></span>
      <button id="autologin-cancel">Cancel</span>
    </div>`;
  const panel = template.content.firstElementChild;
  let message = panel.querySelector("#autologin-message");
  let cancelButton = panel.querySelector("#autologin-cancel");

  message.innerText = `AutoLoginExtension: Auto-Login in ${
    loginDelayMS / 1000
  }s`;

  cancelButton.onclick = () => {
    cancel();
    setPanelMessage("Cancelled Auto-Login");
  };

  document.body.appendChild(panel);
}

function setPanelMessage(msg) {
  let cancelButton = document.body.querySelector("#autologin-cancel");
  if (cancelButton) {
    cancelButton.remove();
  }

  document.body.querySelector("#autologin-message").innerText = msg;
}

function startTimer(loginDelay, action) {
  const timeoutId = window.setTimeout(() => action(), loginDelay);
  return () => window.clearTimeout(timeoutId);
}

console.log("Hello from Auto Login Extension.");

if (document.location.host == "moodle.tu-dortmund.de") {
  if (moodle.notLoggedIn()) {
    browser.storage.local
      .get({
        loginDelay: 2000,
      })
      .then((storedData) => {
        const cancelAction = startTimer(storedData.loginDelay, () => {
          document.location.href = moodle.getLogInInitateURL()
          setPanelMessage("Going to login page...")
        })
        insertAutoLoginPanel(cancelAction, storedData.loginDelay);
      });
  }
}

if (document.location.href.startsWith(ssoLoginPrefix)) {
  const intervalID = window.setInterval(() => {
    const usernameField = document.querySelector('input[id="idToken1"]');
    const passwordField = document.querySelector('input[id="idToken2"]');
    const loginButton = document.querySelector('input[id="loginButton_0"]');
    if (usernameField && passwordField && loginButton) {
      window.clearInterval(intervalID);
      browser.storage.local
        .get({
          credentials: { username: "", password: "" },
        })
        .then((storedData) => {
          let { credentials } = storedData;
          if (!usernameField.value && credentials.username) {
            usernameField.value = credentials.username;
          }
          if (!passwordField.value && credentials.password) {
            passwordField.value = credentials.password;
          }
          if (usernameField.value && passwordField.value) {
            const cancelAction = startTimer(storedData.loginDelay, () => {
              loginButton.click();
              setPanelMessage("Login Form sent, you will be redirected shortly");
            })
            insertAutoLoginPanel(cancelAction, storedData.loginDelay);
          }
        });
    }
  }, 100);
}
